using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


//Go to another scene
public class Teleport : MonoBehaviour
{

    public int sceneIndex = 2;
    public string tag = "Player";


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(tag))
        {
            SceneManager.LoadScene(sceneIndex);
        }
    }
}
