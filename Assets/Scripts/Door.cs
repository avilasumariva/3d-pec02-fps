using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Door script
 * Check if player have in his inventory an item with the name equals at doorKey var.
 * The opening is made disabling components
 */
public class Door : MonoBehaviour
{
    public string doorKey;
    public bool doorState = false;
    public int level;

    public GameObject closedDoor;


    private void OnTriggerStay(Collider other)
    {
        PlayerInventory inventory = other.GetComponent<PlayerInventory>();



        if(inventory != null)
        {
            UIManager.instance.InfoText("Press E to open door " + " You need a key");


            if (Input.GetKeyDown(KeyCode.E))
            {

                GameObject check = null;

                foreach(GameObject i in inventory.inventory)
                {
                    if (i.GetComponent<Item>().name == doorKey)
                    {
                        check = i;
                        OpenDoor();
                        doorState = true;
                        UIManager.instance.InfoText("Level "+level+" door opened");

                        break;
                    }
                }
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            UIManager.instance.InfoText("");

        }
    }

    public void OpenDoor()
    {
        doorState = true;

        closedDoor.SetActive(false);

        foreach(BoxCollider bc in GetComponents<BoxCollider>())    
        {

            bc.enabled = false;

        }


    }


}
