using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


/*Player State script
 * Controls life and shield of player.
 * As long as he has shield left, he will take a tenth of the damage.
 */
public class PlayerHealth : MonoBehaviour
{
    [SerializeField]
    private float maxLife = 10;
    public float life;
    [SerializeField]
    private float maxShield = 10;
    public float shield;

    public float shieldCooldown = 3.0f;
    public bool cooldownActive;

    UIManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        
        uiManager = UIManager.instance;
        life = maxLife;
        shield = maxLife;
    }
    public void Hurt(float damage)
    {
        if (shield > 0)
        {
            shield -= damage;

            life -= damage * 0.1f;

        }
        else
        {
            life -= damage;
        }

        uiManager.RefreshLife(life*(maxLife*0.01f));
        uiManager.RefreshShieldBar(shield*(maxShield*0.01f));

        if (life < 0)
        {
            Death();
        }

    }

    public void Heal(float heal)
    {
        life += heal;
        if (life > maxLife)
            life = maxLife;

        uiManager.RefreshLife(life * (maxLife * 0.01f));

    }

    public void AddShield()
    {
        shield = maxShield;
        uiManager.RefreshShieldBar(shield * (maxShield * 0.01f));

    }

    public void Death()
    {
        GetComponent<FirstPersonController>().enabled = false;
        GameManager.instance.GameOver();
    }


}
