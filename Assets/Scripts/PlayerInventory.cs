using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Control item and weapons of the player.
public class PlayerInventory : MonoBehaviour
{

    public GameObject[] weapons;

    public GameObject activeWeapon;
    public int wheelIndex;

    public List<GameObject> inventory;
    // Start is called before the first frame update
    void Start()
    {
        inventory = new List<GameObject>();

        activeWeapon = weapons[0];
        activeWeapon.SetActive(true);
    }




//For switch weapon, player use the mouse wheel
    void Update()
    {
        if (Input.GetAxisRaw("Mouse ScrollWheel") != 0)
        {
            wheelIndex++;
            SwitchWeapon(wheelIndex);

        }

    }

    public void SwitchWeapon(int input)
    {

        activeWeapon.SetActive(false);


        activeWeapon = weapons[input % weapons.Length];

        activeWeapon.SetActive(true);

    }


    public void ChangeWeapon(int index)
    {
        foreach(GameObject w in weapons)
        {
            w.SetActive(false);

        }

        GameObject weapon = weapons[index];
        Weapon weaponScript = weapon.GetComponent<Weapon>();

        weapon.SetActive(true);
        string ammoText = weaponScript.actualAmmo + "/" + weaponScript.storedAmmo; 
        UIManager.instance.RefreshAmmo(ammoText);

    }

    public void AddItem(GameObject item)
    {

        inventory.Add(item);

    }
}
