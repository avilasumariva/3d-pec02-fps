using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//End of the game, load credits scene
public class Goal : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")){

            GameManager.instance.EndGame();

        }
    }
}
