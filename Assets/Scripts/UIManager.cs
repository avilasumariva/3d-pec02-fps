using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//Control the HUD
public class UIManager : MonoBehaviour
{
    public Slider m_ShieldBar;
    public Slider m_LifeBar;

    public Image m_WeaponImage;
    public TextMeshProUGUI m_Ammo;

    public GameObject HUDScreen;
    public GameObject pauseScreen;
    public GameObject gameOverScreen;

    public GameObject itemsGroup;


    public TextMeshProUGUI infoText;

    #region Singleton
    public static UIManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogWarning("UiManager is already instantiated.");
        }

    }
    #endregion

    public void RefreshShieldBar(float value)
    {
        m_ShieldBar.value = value;

    }

    public void RefreshLife(float value)
    {
        m_LifeBar.value = value;

    }

    public void RefreshAmmo(string ammo)
    {
        m_Ammo.text = ammo;

    }

    public void InfoText(string info)
    {
        infoText.text = info;


    }

    public void AddItem(GameObject item)
    {

       //TODO

    }


}
