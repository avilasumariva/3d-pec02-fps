using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenu : MonoBehaviour
{
    
    public void onTryAgainButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void onMainMenuButton()
    {
        SceneManager.LoadScene(0);
    }

    public void onExitButton()
    {
        Application.Quit();
    }

    public void onResumeButton()
    {
        UIManager.instance.pauseScreen.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void onStartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void onCreditsScene()
    {
        SceneManager.LoadScene(3);
    }

}
