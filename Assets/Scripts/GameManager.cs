using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Script for manage all game states
public class GameManager : MonoBehaviour
{

    #region Singleton
    public static GameManager instance;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogWarning("GameManager is already instantiated.");
        }

    }
    #endregion

    private void Start()
    {
        Time.timeScale = 1.0f;
    }

    public void GameOver()
    {
        Time.timeScale = 0.0f;
        UIManager.instance.gameOverScreen.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void EndGame()
    {
        SceneManager.LoadScene(3);
    }

}


enum GameState
{
    BEGINNING,
    PLAYING,
    PAUSE,
    OVER
}