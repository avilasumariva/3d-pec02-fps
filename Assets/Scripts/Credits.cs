using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Just for move the credits
public class Credits : MonoBehaviour
{

    public GameObject creditText;
    public float speed = 4;



    private void Update()
    {
        creditText.transform.localPosition = transform.localPosition + Vector3.up * Time.deltaTime * speed;



        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);

        }
    }


    

}
