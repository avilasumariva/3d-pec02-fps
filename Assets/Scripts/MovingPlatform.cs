using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*Moving platform
 * Coded for multiuse
 * When movement begin, the platform move through all nodes and back.
 * You can stablish speed, if is active, if need the player on top...
 */
public class MovingPlatform : MonoBehaviour
{
    public bool active;
    public bool needPlayer;
    public bool auto;
    public float speed;
    public float waitTime = 2.0f;
    public Transform[] nodes;

    public bool direction;
    public int index;

    private void Start()
    {
        transform.position = nodes[0].position;
        index = 1;
        direction = true;
    }


    private void OnTriggerExit(Collider other)
    {
       
        if (other.gameObject.tag == "Player")
        {
            other.transform.parent = null;

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        

        if (other.gameObject.tag == "Player")
        {
            other.transform.parent = transform;

        }


        if (needPlayer && active && other.gameObject.tag == "Player")
        {
            StartMovement();

        }
    }

    public void StartMovement()
    {
        StartCoroutine(PlatformMovement(nodes[index]));
    }

    IEnumerator PlatformMovement(Transform next)
    {

        while(Vector3.Distance(transform.position, next.position) > 0.5f && active)
        {
            transform.position = Vector3.MoveTowards(transform.position, next.position, speed * Time.deltaTime);
            yield return null;
        }

        if (direction)
        {
            index++;

            if (index < nodes.Length)
            {
                StartCoroutine(PlatformMovement(nodes[index]));

            }
            else
            {
                direction = false;
                index = nodes.Length - 1;

                if (auto)
                {
                    yield return new WaitForSeconds(waitTime);
                    StartCoroutine(PlatformMovement(nodes[index]));
                }
            }
        }
        else
        {
            index--;

            if (index >= 0)
            {
                StartCoroutine(PlatformMovement(nodes[index]));

            }
            else
            {

                direction = true;
                index = 0;

                if (auto)
                {
                    yield return new WaitForSeconds(waitTime);

                    StartCoroutine(PlatformMovement(nodes[index]));
                }

            }
        }

    }
}
