using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//If player fall here, he die
public class DoomZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerHealth>().Death();

        }
    }
}
