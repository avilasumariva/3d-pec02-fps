using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Bullet prefab code.
 * Used in player and enemies
 * Right now, the game have friend fire
 */
public class Bullet : MonoBehaviour
{
    public float damage= 10;
    public float lifetime = 0.2f;

    private void Start()
    {
        StartCoroutine(BulletLife());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<Enemy>().TakeDamage(damage);

        }

        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerHealth>().Hurt(damage);

        }


        Destroy(gameObject);
    }

    IEnumerator BulletLife()
    {

        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);

    }
}
