using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Active secret enemies when player reaches a zone
public class SurpriseEnemy : MonoBehaviour
{

    public GameObject enemies;


    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
            enemies.SetActive(true);
    }



}
