using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleState : IEnemyState
{

    Enemy enemy;
    float timer = 0;

    public IdleState(Enemy e)
    {
        enemy = e;
    }

    public void StartState()
    {
        enemy.state = EnemyState.PEACE;
        enemy.SwitchFace(0);
        enemy.currentState = enemy.idleState;

    }
    public void UpdateState()
    {
        enemy.playerDistance = Vector3.Distance(enemy.transform.position, enemy.player.position);

        if(enemy.playerDistance < 5)
        {

            GoToAlertState();

        }

        timer += Time.deltaTime;
        if (timer > enemy.timeWander)
        {
            Vector3 newPos = RandomNavSphere(enemy.transform.position, enemy.wanderRadious, -1);
            enemy.agent.SetDestination(newPos);
            timer = 0;
        }

    }

    public void GoToAlertState()
    {
        enemy.alertState.StartState();
    }

    public void GoToIdleState() { }

    public void GoToAttackState()
    {
        enemy.attackState.StartState();
    }


    public void OnTriggerEnter(Collider col)
    {

    }

    public void OnTriggerStay(Collider col) { }
    public void OnTriggerExit(Collider col) { }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    IEnumerator randomPlace()
    {
        Vector3 randDirection = Random.insideUnitSphere * enemy.wanderRadious;
        RandomNavSphere(enemy.transform.position, enemy.wanderRadious, -1);
        yield return new WaitForSeconds(5);


    }
}

