using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Enemy stare you a time after begin to attack.
public class AlertState : IEnemyState
{

    Enemy enemy;
    public AlertState(Enemy e)
    {
        enemy = e;
    }
    public void StartState()
    {

        enemy.transform.LookAt(enemy.player);
        enemy.CountToAttack();
        enemy.currentState = enemy.alertState;
        
    }

    public void UpdateState()
    {
        enemy.transform.LookAt(enemy.player);


        enemy.playerDistance = Vector3.Distance(enemy.transform.position, enemy.player.position);

        if (enemy.playerDistance > 10)
        {

            enemy.idleState.StartState();
        }
    }

    public void GoToAlertState()
    {
    }

    public void GoToIdleState()
    {
        enemy.idleState.StartState();

    }

    public void GoToAttackState()
    {
        enemy.attackState.StartState();

    }

    public void OnTriggerEnter(Collider col)
    {
        
    }

    public void OnTriggerExit(Collider col)
    {
   
    }

    public void OnTriggerStay(Collider col)
    {

    }

}
