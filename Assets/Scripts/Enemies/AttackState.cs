using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IEnemyState
{

    Enemy enemy;

    public AttackState(Enemy e)
    {
        enemy = e;
        
    }

    public void StartState()
    {
        enemy.state = EnemyState.DESTROY;
        enemy.ShootMethod();
        enemy.SwitchFace(1);
        enemy.currentState = enemy.attackState;

    }

    public void GoToAlertState()
    {
        enemy.alertState.StartState();
    }

    public void GoToIdleState()
    {
        enemy.idleState.StartState();
    }
    public void GoToAttackState()
    {
        Debug.Log("Redudant state");
    }

    public void OnTriggerEnter(Collider col)
    {
        
    }

    public void OnTriggerExit(Collider col)
    {
        
    }

    public void OnTriggerStay(Collider col)
    {
        
    }

    public void UpdateState()
    {
        enemy.transform.LookAt(enemy.player);

        enemy.agent.SetDestination(enemy.player.position);


        enemy.playerDistance = Vector3.Distance(enemy.transform.position, enemy.player.position);

        if (enemy.playerDistance > 15)
        {

            enemy.idleState.StartState();

        }

    }
}
