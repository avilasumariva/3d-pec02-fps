using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyState
{

    void StartState();

    void UpdateState();
    void GoToAlertState();
    void GoToIdleState();
    void GoToAttackState();

    void OnTriggerEnter(Collider col);
    void OnTriggerStay(Collider col);
    void OnTriggerExit(Collider col);
}
