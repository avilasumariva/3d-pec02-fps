using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*Enemy script
 * Enemy use state control (Assets/Scripts/Enemies)
 */
public class Enemy : MonoBehaviour
{
    [HideInInspector] public IdleState idleState;
    [HideInInspector] public AlertState alertState;
    [HideInInspector] public AttackState attackState;
    //[HideInInspector] 
    public IEnemyState currentState;

    [Header("Enemy Status")]
    public float heal = 10;
    public float speed = 4;
    public EnemyState state;
    public NavMeshAgent agent;

    public bool canSeePlayer;

    [Header("Enviroment info")]
    public Transform player;
    public float playerDistance;

    [Header("Prefabs")]
    public GameObject lootItem;

    public GameObject bulletPreb;

    public GameObject[] faces;

    [Header("Perception field")]
    public float radius;
    [Range(0, 360)]
    public float angle;

    [Header("State parameters")]
    public float timeAlert = 4;
    public float timeWander = 5;
    public float wanderRadious;
    public Transform firePoint;
    public bool canShoot;
    public float shootForce;
    public float cadence;



    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        idleState = new IdleState(this);
        alertState = new AlertState(this);
        attackState = new AttackState(this);

        currentState = idleState;
    }

    private void Update()
    {
        currentState.UpdateState();
    }

    public void TakeDamage(float damage)
    {
        if(currentState != attackState)
        {
            attackState.StartState();
        }

        heal -= damage;
        if (heal < 0)
        {
            Death();
        }


    }

    //Left one object at die
    public void Death()
    {
        if (lootItem != null)
        {
            GameObject loot = Instantiate(lootItem, transform.position, transform.rotation);
            loot.transform.parent = null;
        }

        Destroy(gameObject);
    }

    //Just stetic
    public void SwitchFace(int index)
    {
        foreach (GameObject s in faces)
        {
            s.SetActive(false);
        }

        faces[index].SetActive(true);
    }

    public void CountToAttack()
    {

        StartCoroutine(TimeToAttack());
    }

    IEnumerator TimeToAttack()
    {
        yield return new WaitForSeconds(timeAlert);
        attackState.StartState();
    }

    public void Shoot()
    {

        GameObject b = Instantiate(bulletPreb, firePoint.transform);
        b.transform.parent = null;

        b.GetComponent<Rigidbody>().AddForce(firePoint.forward * shootForce, ForceMode.Force);
    }

    //For start coroutine from state script
    public void ShootMethod()
    {
        StartCoroutine(ShootRoutine());
    }

    IEnumerator ShootRoutine()
    {
        while(state==EnemyState.DESTROY)
        {
            yield return new WaitForSeconds(cadence);
            SwitchFace(2);
            Shoot();

            yield return new WaitForSeconds(0.2f);
            SwitchFace(1);

        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerHealth>().Hurt(1);
        }
    }


}

public enum EnemyState{
    PEACE,
    SEEK,
    DESTROY

}
