using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Weapon script for the prefabs
 * Control cadence, bullet to instantiate, reload time, audio clips...
 */
public class Weapon : MonoBehaviour
{

    public TypeWeapon type;
    public GameObject weaponPreb;

    public int storedAmmo;
    public int chargeCapacity;
    public int actualAmmo;

    public float reloadTime;
    public bool automatic;
    public float cadence;

    public GameObject bullet;
   // public GameObject firePoint;
    public Transform firePoint;
    public AudioSource fireSound;
    public AudioSource reloadSound;
    public float shootForce;
    public GameObject effect;

    public bool canShoot;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0) && (actualAmmo > 0) && canShoot && !automatic)
        {
            StartCoroutine(Shoot());
        }


        if (Input.GetKey(KeyCode.Mouse0) && (actualAmmo > 0) && canShoot && automatic)
        {
            StartCoroutine(Shoot());

        }


        if (Input.GetKey(KeyCode.R) && storedAmmo > 0)
        {

            StartCoroutine(Reload());


        }

    }

    IEnumerator Shoot()
    {
        canShoot = false;
        StartCoroutine(ShootEffect());
        actualAmmo--;

        string ammoText = actualAmmo + "/" + storedAmmo;
        UIManager.instance.RefreshAmmo(ammoText);

        GameObject b = Instantiate(bullet, firePoint.transform);
        b.transform.parent = null;

        b.GetComponent<Rigidbody>().AddForce(firePoint.forward * shootForce, ForceMode.Force);

        fireSound.Play();

        if (actualAmmo == 0)
        {
            reloadSound.Play();
        }



        yield return new WaitForSeconds(cadence);
        canShoot = true;
    }

    IEnumerator ShootEffect()
    {
        effect.SetActive(true);
        yield return new WaitForSeconds(0.05f);
        effect.SetActive(false);


    }

    IEnumerator Reload()
    {

        canShoot = false;
        yield return new WaitForSeconds(reloadTime);

        int bullets = chargeCapacity - actualAmmo;

        if(storedAmmo < bullets)
        {
            actualAmmo += storedAmmo;
            storedAmmo = 0;

        }
        else
        {
            storedAmmo -= bullets;
            actualAmmo += bullets;

        }



        string ammoText = actualAmmo + "/" + storedAmmo;
        UIManager.instance.RefreshAmmo(ammoText);


        canShoot = true;

    }

    public void AddAmmo(int newAmmo)
    {

        storedAmmo += newAmmo;
        string ammoText = actualAmmo + "/" + storedAmmo;
        UIManager.instance.RefreshAmmo(ammoText);
    }


}


public enum TypeWeapon
{
    PISTOL,
    RIFLE,
    SHOTGUN
}