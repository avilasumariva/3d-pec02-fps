using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Inventory item code.
 * If press E near, the player add the item at his inventory list.
 * 
 */

public class Item : MonoBehaviour
{

    public string itemCode;

    private void OnTriggerStay(Collider other)
    {

        PlayerInventory inventory = other.GetComponent<PlayerInventory>();
        if (inventory != null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {

                inventory.AddItem(this.gameObject);
                UIManager.instance.InfoText("You get " + name);


                //borrar el objeto del escenario? �-�

                transform.position = new Vector3(1000, 1000, 1000);


            }

        }

        UIManager.instance.InfoText("Press E for take " + name); ;

    }

    private void OnTriggerExit(Collider other)
    {
        UIManager.instance.InfoText(""); ;

    }


}
