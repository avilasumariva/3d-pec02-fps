using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Basic item.
 * The player pick up at touch it.
 * TODO
 * The ammo right now recharge the equipped weapon. Create different ammo for different weapons.
 */
public class AmmoItem : MonoBehaviour
{ 
    public int amount = 10; 

    private void OnTriggerEnter(Collider other)
    {
        PlayerInventory player = other.GetComponent<PlayerInventory>();

        if(player != null)
        {

            player.activeWeapon.GetComponent<Weapon>().AddAmmo(amount);
            
            Destroy(gameObject);

        }
    }


}
