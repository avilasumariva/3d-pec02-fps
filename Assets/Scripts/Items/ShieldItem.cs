using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Basic item.
 * The player pick up at touch it.
 * Restore all the shield
 */
public class ShieldItem : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerHealth player = other.GetComponent<PlayerHealth>();

        if (player != null)
        {
            player.AddShield();

            Destroy(this.gameObject);
        }

    }
}
