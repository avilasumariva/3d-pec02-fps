using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Basic item.
 * The player pick up at touch it.
 * Restore life points
 */
public class LifeItem : MonoBehaviour
{
    [SerializeField]
    float _gainLife = 10;

    private void OnTriggerEnter(Collider other)
    {
        PlayerHealth player = other.GetComponent<PlayerHealth>();

        if(player != null)
        {
            player.Heal(_gainLife);

            Destroy(this.gameObject);
        }
        
    }
}
