using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Just stetic.
public class RotateItem : MonoBehaviour
{
    [SerializeField]
    Vector3 _rotation;
    [SerializeField]
    float _speed;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(_rotation * _speed * Time.deltaTime);
    }
}
