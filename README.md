# FPS - Anacrológisis
## UOC Programación de videojuegos 3D PEC 02

Este proyecto resulta en el desarrollo de un prototipo de videojuego FPS usando el motor Unity (2020.3.17f1).

## Cómo jugar
La demo contiene cuatro escenas(Assets/Scenes): 00-MainMenu, 01-ForestScene, 02-InsideScene y 03-Credits.

### MainMenu
![MainMenu](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/imag01-menu.jpg)


Start Demo -> comienza el juego desde la escena 01 del bosque.
Credits -> pantalla de créditos
Exit -> salir

### Controles
El esquema de control usa teclado y ratón.
WASD -> movimiento
Space -> salto
R -> recarga
E -> acción cirscunstancial. Recoger llaves y abrir puertas.
RatónIzquierda -> disparar
RatónRueda -> cambiar de arma

### Forest
El primer nivel se desarrolla en una zona montañosa generado con la herramienta Terrain. El objetivo del jugador consiste en llegar hasta el castillo señalizado por una fuente de luz.

![Forest](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image02-forest.jpg)


### Inside
El segundo nivel sucede dentro de una mazmorra de estética espacial. Para superar el nivel, el jugador tiene que conseguir las llaves correspondientes para cada puerta (La puerta amarilla con la llave amarilla).
![Inside](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image03-inside.jpg)

### Armas
El jugador tiene dos armas disponibles controladas por el script Weapon.sc(Assets/Scripts/Weapon). Cada una tiene diferencias de cadencia, capacidad de cargador, etc.
La pistola usa inputbuttondown, o sea, se necesita pulsar una vez por disparo, sin embargo, el rifle de asalto usa inputbutton, así que el disparo es continuo.

Ambos funcionan instanciando un objeto Bullet, que se impulsa en la dirección hacia la que mira el jugador, cuando este objeto choca con otro que pueda ser dañado, le impondrá un daño. Cada vez que se dispara, se reproduce un efecto de sonido, distinto para cada arma.
Cuando la cantidad de munición cargada llega a cero, se anima al jugador a recargar con un clip de audio (R).

### Salud
La condición de salud del jugador se gestiona en el script PlayerHealth.sc (Assets/Scripts/PlayerHealth).
Aqui se establece la cantidad de vida y escudo, y será donde se establezan el daño o la restauración de cada factor.

Mientras el escudo esté activo, éste absorberá el impacto, y sólo se le inflingirá un 10% del daño a la vida.

```
public void Hurt(float damage)
    {
        if (shield > 0)
        {
            shield -= damage;

            life -= damage * 0.1f;

        }
        else
        {
            life -= damage;
        }

        uiManager.RefreshLife(life*(maxLife*0.01f));
        uiManager.RefreshShieldBar(shield*(maxShield*0.01f));

        if (life < 0)
        {
            Death();
        }

    }
```

### HUD
Se ha generado un objeto UIManager (Assets/Scripts/UIManager.sc) para gestionar la actualización de los elementos del HUD.
Cada vez que se cambian los valores de según qué parametros(vida, escudo o munición), se ejecuta funciones "refresh" en la instancia del UIManager, al cual están enlazado los elementos de panel del Canvas establecido como HUD.

```
public void RefreshAmmo(string ammo)
{
    m_Ammo.text = ammo;

}
```

![HUD](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image04-hud.jpg)


### Plataformas móviles
En el segundo nivel, se ha desarrollado el elemento de las plataformas móviles. Estas consisten en un objeto que contiene una plataforma y una serie de nodos por los que debe pasar una vez activada esta(Assets/Prefabs/MovingPlatform). El script lo adjunta el objeto que sea la plataforma (Assets/Scripts/MovingPlatform), luego hay que establecer los nodos en la lista correspondientes, y distintos parámetros como si está activa, la velocidad, etc.

![Platform](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image05-platform.jpg)

```
IEnumerator PlatformMovement(Transform next)
   {

       while(Vector3.Distance(transform.position, next.position) > 0.5f && active)
       {
           transform.position = Vector3.MoveTowards(transform.position, next.position, speed * Time.deltaTime);
           yield return null;
       }

       if (direction)
       {
           index++;

           if (index < nodes.Length)
           {
               StartCoroutine(PlatformMovement(nodes[index]));

           }
           else
           {
               direction = false;
               index = nodes.Length - 1;

               if (auto)
               {
                   yield return new WaitForSeconds(waitTime);
                   StartCoroutine(PlatformMovement(nodes[index]));
               }
           }
       }
       else
       {
           index--;

           if (index >= 0)
           {
               StartCoroutine(PlatformMovement(nodes[index]));

           }
           else
           {

               direction = true;
               index = 0;

               if (auto)
               {
                   yield return new WaitForSeconds(waitTime);

                   StartCoroutine(PlatformMovement(nodes[index]));
               }

           }
       }

   }
   ```

### Puertas
En el nivel de interior, se ha implementado un tipo de objeto Door qué sólo se podrá abrir si el jugador posee en su inventario un tipo de objeto en específico.
Por razones de simplificación, abrir la puerta deshabilitará el objeto "collider" que impide pasar al jugador, que será del mismo color que la llave que necesita.

Assets/Scripts/Door.sc
```
private void OnTriggerStay(Collider other)
  {
      PlayerInventory inventory = other.GetComponent<PlayerInventory>();
      if(inventory != null)
      {
          UIManager.instance.InfoText("Press E to open door " + " You need a key");
          if (Input.GetKeyDown(KeyCode.E))
          {
              GameObject check = null;

              foreach(GameObject i in inventory.inventory)
              {
                  if (i.GetComponent<Item>().name == doorKey)
                  {
                      check = i;
                      OpenDoor();
                      doorState = true;
                      UIManager.instance.InfoText("Level "+level+" door opened");

                      break;
                  }
              }
          }

      }
  }
  ```
En este caso, la puerta tiene un parametro de tipo string donde establecer el nombre del objeto que actuará de llave. Si el objeto existe en el inventario, la puerta se abre interactuando cerca de ella con el tecla E.


![Door](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image06-door.jpg)


### Enemigos
Se ha poblado los niveles con Enemigos (Assets/Scripts/Enemies/Enemy.sc). El comportamiento se controlará con una máquina de estado implementada desde cada uno de sus estados:

IdleState -> el personaje usará el componente NavMesh para ir eligiendo puntos aleaotorios a los que ir desplazandose.

AlertState -> Si el jugador se encuentra cercad el enemigo, éste se pondrá en estado de alerta, y se girará para mantenerse observandolo. Si el jugador se aleja lo suficiente mientras esté en este estado, volverá a vagar por el escenario; si pasa demasiado tiempo en su rango, el enemigo pasará al ataque.

AttackState -> El enemigo instanciará objetos Bullet en dirección al jugador con una frecuencia determinada.

![Enemies](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image07-enemies.jpg)

### Consumibles
Se han desarrollado tres tipos de objetos que se consumen nada más tocarlos el jugador. Éstos son (Assets/Prefabs/Items/)LifeItem, ShieldItem y Ammo.

![Items](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image08-items.jpg)

LifeItem -> Restaura una cantidad de puntos de vida en el jugador.
```
public class LifeItem : MonoBehaviour
{
    [SerializeField]
    float _gainLife = 10;

    private void OnTriggerEnter(Collider other)
    {
        PlayerHealth player = other.GetComponent<PlayerHealth>();

        if(player != null)
        {
            player.Heal(_gainLife);

            Destroy(this.gameObject);
        }

    }
}
```

ShieldItem -> Restaura completamente la barra de escudo
```
public class ShieldItem : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerHealth player = other.GetComponent<PlayerHealth>();

        if (player != null)
        {
            player.AddShield();

            Destroy(this.gameObject);
        }

    }
}
```

AmmoItem -> Entrega una cantidad de balas al arma activada en ese momento. (TODO: establecer tipos de munición para tipos de armas)
```
public class AmmoItem : MonoBehaviour
{
    public int amount = 10;

    private void OnTriggerEnter(Collider other)
    {
        PlayerInventory player = other.GetComponent<PlayerInventory>();

        if(player != null)
        {

            player.activeWeapon.GetComponent<Weapon>().AddAmmo(amount);

            Destroy(gameObject);

        }
    }


}
```

### Game Over Screen

Si la vida del jugador llega a cero, o cae en ciertas zonas (DoomZone) el juego despliega la pantalla de Game Over, desde la cual puede reintentarlo o salir al menú principal.


![Items](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image09-gameover.jpg)


### Puntos opcionales

-Se ha establecido algunos efectos de sonido (disparos, aviso de recarga) y música ambiental(menú, primera escena y créditos), creando un AudioMixer para el proyecto y asignando a cada clip su grupo correspondiente.
-Se han modificado algunos enemigos para hacerlos más lentos, grandes y resistentes. En el caso del enemigo gigante de la segunda escena, dispara munición en lugar de balas.
-Se han añadido dead zone, en el primer escenario, imitando masas de agua, y en el segundo, para evitar que el jugador caiga al infinito.

## Video Gameplay anotado
https://youtu.be/dT7Dv0cXFVo


![End](https://gitlab.com/avilasumariva/3d-pec02-fps/-/raw/main/imgs/image10-end.jpg)
