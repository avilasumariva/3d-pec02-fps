# Programación en Unity 3D

# PAC 2: First Person Shooter

La asignatura _Programación en Unity 3D_ se evalúa con tres prácticas de evaluación continua y una práctica final.

### Fechas importantes de la PAC2

Fecha de publicación del enunciado: 20/03/2022

Fecha de entrega: 17/04/2022

Fecha de calificación: 04/05/2022


### Cálculo de la nota final
La nota final se calculará en base a las notas individuales de cada PAC. Al ser una evaluación continua, la evolución del alumno durante la asignatura y su participación en los debates y actividades propuestas en el aula también se tendrá en cuenta a la hora de fijar el resultado final.

### Librerías necesarias para hacer las prácticas
Esta práctica la vamos a realizar usando Unity 3D de la versión **2020.3.17f1 LTS**.

### Sistemas operativos aceptados
Os recomendamos que realicéis las prácticas en un entorno Windows o Mac OS. Si necesitáis licencias de Windows, Microsoft Visual Studio o cualquier otro software, comentadlo en el foro de la asignatura y os indicaremos si desde la UOC os las podemos proporcionar.

### ¿Qué se tiene que entregar?
Se deberá subir el proyecto funcionando sin errores al GitLab, y que este contenga TODOS los siguientes elementos:

- Un TAG identificativo (PAC2) en la versión final que queréis que sea evaluada.
- Un fichero README.md que explique cómo ejecutar el proyecto si es que hay algún requisito extra, así como unas instrucciones que expliquen los controles del juego o si hay que hacer alguna acción específica en concreto para poder jugarlo.
- En ese mismo Readme, se deberá incluir una descripción explicando qué partes habéis implementado y cómo lo habéis hecho.
- Un link a un vídeo que enseñe el funcionamiento de todos los puntos que se han implementado donde se os escuche a vosotros explicando las funcionalidades creadas.


### Descripción del juego propuesto

Siguiendo el hilo del documento &quot;First Person Shooter&quot;, os proponemos que implementéis un juego en primera persona en el que el jugador tenga que superar dos zonas diferentes de juego. En estos dos zonas deberá enfrentarse a &quot;enemigos&quot; y superar algún puzzle para progresar en los niveles.

Los puntos básicos que debéis implementar son:

1. La parte inicial del escenario deberá estar ubicada en un terreno montañoso, pudiendo también hacer partes urbanas, y luego pasará a ser un entorno cerrado dentro de algún tipo de edificio o instalación.
2. El personaje deberá tener dos tipos de arma. Una más pequeña, de cadencia lenta y precisa como podría ser una pistola, y otra más de largo alcance y de fuego rápido como podría ser una ametralladora.
3. A parte de su barra de vida, el personaje tendrá también una barra de escudo. Cuando le disparen, si tiene aún escudo, será el escudo quien reciba la mayor parte del daño, pero aun así deberá perder un poco de vida. Cuando el escudo llegue a 0, la vida se llevará el 100% del daño.
4. Tanto la cantidad de vida, cómo de escudo, el tipo de arma y la munición deberán siempre verse por pantalla (HUD).
5. El escenario deberá tener plataformas móviles, ya sean ascensores o desplazamientos horizontales.
6. Deberán haber puertas que no puedan abrirse si el player no tiene la llave adecuada. Así se irán generando puzles.
7. Deberá haber &quot;enemigos&quot;  que patrullen por la escena y que si te ven se pongan a dispararte. Si pasas cerca suyo pero no te han visto (simulando que han oído tus pasos) deberán dar una vuelta de 360 grados buscándote antes de seguir con su patrulla si no te ven.
8. Esparcidos por el escenario deberán haber ítems de &#39;vida&#39;, &#39;escudo&#39; y &#39;munición&#39;. Los enemigos al morir deberían dejar alguno en el suelo. También podrían dejar llaves.
9. El juego deberá tener pantalla de game over y que al morir podamos reiniciar el nivel.

Los puntos que valoraremos son:

1. Definir una buena estructura de datos para guardar la información del juego.
2. Claridad y sencillez en el código.
3. Buena documentación y explicación del trabajo realizado.

Los puntos optativos que podéis implementar una vez implementados los puntos anteriores son:

1. Añadir más niveles.
2. Añadir nuevas armas como granadas o una escopeta.
3. El juego deberá estar sonorizado completamente. Pasos, coger un ítem, ser herido o herir, disparar, saltar, caer, abrir puerta…
4. Algunas puertas podrían tener interruptores que una vez abiertas se cierren a los pocos segundos. Con eso se podrían generar puzles de velocidad para llegar a tiempo a su interior.
5. Crear diferentes tipos de enemigos. Quizá algunos más fuertes y lentos, o algún sniper que se vea su puntero laser, o con diferentes tipos de armas.
6. Cuando muere un enemigo hacer que desaparezca poco a poco con un fade.
7. Añadir dead zones, zonas como lava, o precipicios.
8. Incorporarle lógica de checkpoints que si el player muere no tenga que empezar desde el principio.

Como punto opcional extra teneis la opción de darle un empuje a nivel visual y hacer el proyecto utilizando el [HDRP](https://unity.com/es/srp/High-Definition-Render-Pipeline) (High Definition Render Pipeline).

Para implementar este último punto, os recomendamos las siguientes fuentes de información oficiales donde encontraréis todo lo necesiario para empezar a trabajar con el HDRP:

- [Unity Documentation - How to](https://unity.com/es/how-to/getting-started-high-definition-render-pipeline-hdrp-games)

- [Unity Documentation - Manual](https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@7.1/manual/index.html)

- [Unity Documentation - Script Refence](https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@7.1/api/index.html)

- [Unity Documentation - Unity Learn - Up and Runniing with HDRP](https://learn.unity.com/project/up-and-running-with-hdrp)

- [Unity Learn - HDRP Lights](https://learn.unity.com/tutorial/introduction-to-hdrp-lights)

- [Unity Learn - HDRP Materials 1](https://learn.unity.com/tutorial/creating-hdrp-materials)

- [Unity Learn - HDRP Materials 2](https://learn.unity.com/tutorial/hdrp-material-properties-2019-3)

- [EXTRA: Upgrading to HDRP](https://docs.unity.cn/Packages/com.unity.render-pipelines.high-definition@7.5/manual/Upgrading-To-HDRP.html)

- [EXTRA: Unity Graphics Github](https://github.com/Unity-Technologies/Graphics)

- [EXTRA: Unity BLOG - HDRP Scene Template](https://blog.unity.com/technology/explore-learn-and-create-with-the-new-hdrp-scene-template)

